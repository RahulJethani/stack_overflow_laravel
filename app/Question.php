<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Question extends Model
{
    protected $guarded = [];

    /*
    * MUTATORS - These are special functions which have setXXXAttribute($value)
    */
    public function setTitleAttribute($title){
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);//this function is used to take the title value and helps to put it in slug as the url wants it.
    }
    
    /*
    * ACCESSORS - These are special functions which have getXXXAttribute($value)
    */
    public function getUrlAttribute(){
        return "questions/{$this->slug}";
    }

    public function getCreatedDateAttribute(){
        return $this->created_at->diffForHumans();
    }

    public function getAnswersStyleAttribute(){
        if($this->answers_count > 0){
            if($this->best_answer_id){
                return "has-best-answer";
            }
            return "answered";
        }
        return "unanswered";
    }
    /*
    *Relationship methods
    */
    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }

    
    public function answers() {
        return $this->hasMany(Answer::class);
    }

    // HELPER FUNCTIONS

    public function markBestAnswer(Answer $answer){
        // dd($answer);
        $this->best_answer_id = $answer->id;
        $this->save();
    }

}
